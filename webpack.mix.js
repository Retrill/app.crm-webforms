const mix = require('laravel-mix');
let path = require('path');
/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

/* mix.js('resources/js/app.js', 'public/js')
    .postCss('resources/css/app.css', 'public/css', [
        //
    ]); */


mix.js('resources/js/app.js', 'public/js/bundle.js')
    .vue({runtimeOnly: true})
    // .postCss('resources/css/app.css', 'public/css', [])
    .alias({
        vue$: path.join(__dirname, 'node_modules/vue/dist/vue.esm-bundler.js')
    });

// Stop laravel-mix from generating license files:
mix.options({
    terser: {
        extractComments: false,
    }
});
