<?php

use App\Http\Controllers\DonorHosts;
use App\Http\Controllers\Matches;
use App\Http\Controllers\Receiver;
use App\Models\Consumer\DonorHost;
use Fruitcake\Cors\HandleCors;
use Glu\B24RestSdk\Client\Consumer as ConsumerRestClient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::match(['get', 'post'], '/dump', function (Request $request) {
    \dump($request->toArray());
    // phpinfo();
    exit;
});

Route::match(['get', 'post'], '/receive', Receiver::class)->middleware(HandleCors::class);

Route::resource('/hosts', DonorHosts::class)->only(['store', 'update', 'show', 'destroy'])->middleware('checkToken');
Route::post('/hosts/{id}/interact', [DonorHosts::class, 'interact'])->middleware('checkToken');

Route::resource('/hosts/{id}/options/matches', Matches::class)->only(['index', 'store'])->middleware('checkToken');
// we should use post insted of get to force CSRF-protection check
// Route::post('/hosts/{id}/options/matches', [Matches::class, 'index']); //->middleware('checkToken');

Route::match(['get', 'post'], '/install', function (Request $request) {
    return \view('layouts.master', [
        'r' => $request->all([
            'AUTH_ID', 'AUTH_EXPIRES', 'REFRESH_ID',
            'member_id', 'DOMAIN', 'LANG', 'APP_SID',
        ]),
        'placement' => [
            'type' => $request->input('PLACEMENT'),
            'options' => (array) \json_decode($request->input('PLACEMENT_OPTIONS'), true),
        ],
        'restricted' => 0,
        'donors' => \collect([]),
    ]);
});
Route::get('/', function () {
    return view('layouts.greetings');
});
Route::match(['get', 'post'], '/{anything?}', function (Request $request, ConsumerRestClient $client) {
    // \session(['foo' => 'bar2']);
    /* if(!$client->isAuthorized()){
        // we should generate access restricted response here
        return response()->json(['error' => 'Not authorized.'], 403);
    } */
    // \cookie()->queue("test2", 'testv', 60*24, null, null, null, true, false, 'lax');
    // \setcookie('test2', '2435rsjdfgsjdfklashdfadsf', \time() + 3600 * 24, '/', '', false, true);

    /* \setcookie('testn', 'testv2', [
        'expires' => \time() + 24* 3600,
        'path' => '/',
        'secure' => true,
        'httponly' => true,
        'samesite' => ''
    ]); */

    // \dump($request->cookie());
    // \dump($_COOKIE);
    // \dd($request->session()->getId());
    $isAuthorized = $client->isAuthorized();

    if (!$isAuthorized && $request->expectsJson()) {
        return response()->json(['error' => 'Not authorized.'], 403);
    }
    // $isAuthorized = true;

    return \view('layouts.master', [
        'r' => $request->all([
            'AUTH_ID', 'AUTH_EXPIRES', 'REFRESH_ID',
            'member_id', 'DOMAIN', 'LANG', 'APP_SID',
        ]),
        'placement' => [
            'type' => $request->input('PLACEMENT'),
            'options' => (array) \json_decode($request->input('PLACEMENT_OPTIONS'), true),
        ],
        'restricted' => (int) !$isAuthorized,
        'donors' => $isAuthorized ? DonorHost::select('id', 'host', 'title', 'stage')->get() : \collect([]),
    ]);
})->where('anything', '^(?!install\/\d)(?!dump)[\/\w\.-]*');

/*
Route::match(['get', 'post'], '/install', function () {
    return view('install');
});

Route::match(['get', 'post'], '/', function () {
    return view('redirect');
});
*/
