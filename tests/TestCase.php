<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Glu\B24AppBackground\Tests\Traits\MysqlConsumerable;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, MysqlConsumerable;
}
