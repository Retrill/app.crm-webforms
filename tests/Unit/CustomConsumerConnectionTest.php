<?php

namespace Tests\Unit;

// use PHPUnit\Framework\TestCase;
use Tests\TestCase;
use Glu\B24AppBackground\Tests\Traits\CreatesConsumerConnection;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Support\Str;
use App\Models\Consumer\DonorHost;

class CustomConsumerConnectionTest extends TestCase
{
    use DatabaseMigrations, CreatesConsumerConnection;

    public function testAppSpecificConsumerTablesShouldBeMigrated(){
        $this->createConsumerConnection();
        $donorHost = DonorHost::create([
            'host' => 'mytesthost.ru'
        ])->refresh();
        $this->assertIsInt($donorHost->id, 'Donor host should be saved');
        $this->assertEmpty($donorHost->key2enc);
        $this->assertEmpty($donorHost->key2dec);
        $this->assertEquals('initiated', $donorHost->stage);
    }
}
