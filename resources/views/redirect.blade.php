<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>App</title>
</head>
<body>
    @php $r = \request()->all(); @endphp
    <form id="basic" method="post" action="https://72-brucite.wfdemo.ru/local/modules/webformat.formtransmitter/view/app.php">
        @foreach ($r as $key => $value)
            <input type="hidden" name="{{ $key }}" value="{{ $value }}" />
        @endforeach
        Моя формочка!
    </form>
    <script>
        var targetForm = document.getElementById('basic');
        targetForm.setAttribute('target', window.name);
        // console.log('parent before redirect: ', parent);
        // console.log('parent domain before redirect: ', parent.domain);
        targetForm.submit();
    </script>
</body>
</html>
