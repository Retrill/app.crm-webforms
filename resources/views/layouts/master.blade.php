<!DOCTYPE html>
<html lang="{{ $r['LANG'] ?? 'ru' }}">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Application</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link rel="stylesheet" href="/css/app.css">
</head>

<body>
    <div id="app">
        <app :credentials='@json($r)'
            :placement='@json($placement)'
            :restricted="{{ $restricted }}"
            :donor-hosts='{{ $donors->toJson() }}'
            app-host='https://{{ request()->server('SERVER_NAME') }}'
        />
    </div>
    <script>
        window.foundation = {
            csrfToken: '{{ csrf_token() }}'
        }
    </script>
    <script src="//api.bitrix24.com/api/v1/"></script>
    <script src="/js/bundle.js?<?=\mt_rand(1, \mt_getrandmax())?>"></script>
</body>
</html>
