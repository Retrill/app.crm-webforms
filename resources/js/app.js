require('./bootstrap');
import { createApp } from 'vue';
import { createI18n } from 'vue-i18n';
import App from './app.vue';
import { router } from './router.js';
import { storeSymbol, createStore } from './store';
import '@imengyu/vue3-context-menu/lib/vue3-context-menu.css';
import ContextMenu from '@imengyu/vue3-context-menu';
import { ClickOutside } from 'webformat-jse';

import ru from './locale/ru.json';
import en from './locale/en.json';

// BX24.init(function(){
  createApp({components: {App}})
  // createApp(App)
  .use(createI18n({
    legacy: false,
    locale: document.getElementsByTagName('html')[0].getAttribute('lang'),
    fallbackLocale: 'ru',
    messages: { ru, en }
  })).use(router).use(ContextMenu)
  .provide(storeSymbol, createStore())
  .directive("click-outside", ClickOutside)
  .mount('#app');
// });


// console.log('____________', _);