import {createRouter, createWebHistory} from 'vue-router'
import routes from './routes.js'
// import * as VueRouter from 'vue-router';

export const router = createRouter({
    history: createWebHistory(), //createWebHashHistory(),
    base: '/',
    routes,
    linkActiveClass: 'active',
    linkExactActiveClass: 'active-exact'
})
