import Basic from './basic';

export default class extends Basic{
    type = 'smart-process';
    _specificationMethodName = 'crm.item.fields';
    selectOptionTpl = '{{ title }}&nbsp;[#{{ entityTypeId }}]'

    _messages = { ru: {
        title: 'смарт-процесс'
    } };

    loadEntities(){
        return this.b24query('crm.type.list', {
            order: { title: "ASC" },
            filter: { isClientEnabled: "Y" }
        }).then(payload => {
            // _.forEach(payload.types, entity => entity.specification = []);
            Object.assign(this._entities, _.keyBy(payload.types, 'id'));
            this._initSpecification(this._entities);
            return this._entities;
        });
    }

    _loadSpecification(uniqueKey){
        let entityTypeId = this._entities[uniqueKey].entityTypeId;
        return this.b24query(this._specificationMethodName, { entityTypeId }).then(payload => {
            console.log('smart process raw specificaton', payload);
            let tmpShuffledSpecification = [];
            for (const [code, item] of Object.entries(payload.fields)) {
                if(item.upperName == 'ID'){ continue; }
                tmpShuffledSpecification.push({
                    // code: item.upperName || code,
                    code: code,
                    title: (('formLabel' in item) && item.formLabel.length) ? item.formLabel : item.title,
                    type: item.type,
                    isRequired: item.isRequired,
                    isMultiple: item.isMultiple
                })
            }
            return _.sortBy(tmpShuffledSpecification, 'title');
        });
    }

    getEntitySubfilter(uniqueKey){
        return {
            id: uniqueKey,
            entityTypeId: this._entities[uniqueKey].entityTypeId
        };
    }
}
