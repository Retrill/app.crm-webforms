import { http, I18nClass } from 'webformat-jse';
import { reactive } from 'vue';

export default class{
    type = 'unspecified';
    uniqueBy = 'id';
    selectOptionTpl = '{{ title }}&nbsp;[#{{ id }}]'
    _specificationMethodName = '';

    _credentials = {};
    _restEndpoint = '';
    _entities = reactive({});
    _i18n;
    _messages = { ru: { title: 'неопознанный объект' } };

    constructor(credentials, baseUrl) {
        this._credentials = credentials;
        this._baseUrl = baseUrl;
        this._restEndpoint = 'https://' + credentials.DOMAIN + '/rest/';
    }

    _loadSpecification(uniqueKey){
        return this.b24query(this._specificationMethodName).then(payload => {
            let tmpShuffledSpecification = [];
            for (const [code, item] of Object.entries(payload)) {
                if(code == 'ID'){ continue; }
                tmpShuffledSpecification.push({
                    code: code,
                    title: (('formLabel' in item) && item.formLabel.length) ? item.formLabel : item.title,
                    type: item.type,
                    isRequired: item.isRequired,
                    isMultiple: item.isMultiple
                })
            }
            return _.sortBy(tmpShuffledSpecification, 'title');
        });
    }

    _initSpecification(obj){
        _.forEach(obj, entity => entity.specification = reactive({}));
    }

    loadEntities(){
        return new Promise((resolve, reject) => {
            resolve(this._initSpecification(this._entities));
        })
    }

    getSpecification(uniqueKey, immediate){
        if(immediate){
            return this._entities[uniqueKey].specification;
        }
        return new Promise((resolve, reject) => {
            if(_.size(this._entities[uniqueKey].specification) > 0){
                resolve(this._entities[uniqueKey].specification)
            }else{
                this._loadSpecification(uniqueKey).then((specification) => {
                    console.log('loaded specification: ', specification)
                    Object.assign(this._entities[uniqueKey].specification, specification);
                    resolve(this._entities[uniqueKey].specification);
                })
            }
        })
    }

    fieldsetMethodName() {
        return this._fieldsMethodName;
    }

    listFields(){
        return this.b24query(this._fieldsMethodName).then(payload => {
            let tmpShuffledSpecification = [];
            for (const [code, item] of Object.entries(payload)) {
                if(code == 'ID'){ continue; }
                tmpShuffledSpecification.push({
                    code: code,
                    title: (('formLabel' in item) && item.formLabel.length) ? item.formLabel : item.title,
                    type: item.type,
                    isRequired: item.isRequired,
                    isMultiple: item.isMultiple
                })
            }
            return _.sortBy(tmpShuffledSpecification, 'title');
        });
    }

    b24query(methodName, data){
        if(!data) { data = {}}
        data.auth = this._credentials.AUTH_ID;
        let url = this._restEndpoint + methodName + '.json';
        return http.get(url, {
            params: data,
            transformRequest: [
                (data, headers) => {
                    delete headers.common['X-CSRF-TOKEN'];
                    delete headers.common['X-Requested-With'];
                    return data;
                }
            ]
        }).catch(response => {
            console.warn('[basic class] This buggy situation #P3s7kHYGOUOi should be treated correctly! ', response, 'entity object: ', this);
        });
    }

    localization(lang){
        return this._messages[lang];
    }

    getEntitySubfilter(uniqueKey){
        let subfilter = {};
        subfilter[this.uniqueBy] = uniqueKey;
        return subfilter;
    }
}
