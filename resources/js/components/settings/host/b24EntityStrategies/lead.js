import Basic from './basic';

export default class extends Basic{
    type = 'lead';
    _specificationMethodName = 'crm.lead.fields';
    _messages = { ru: {
        title: 'лид'
    } };

    loadEntities(){
        this._entities[0] = {
            id: 0,
            title: 'лид'
        }
        return super.loadEntities();
    }
}
