// import HomePage from './layouts/home.vue'
import InstallPage from '../../vendor/glu/b24.app.background/src/resources/js/layouts/install.vue';
import RestrictionPage from '../../vendor/glu/b24.app.background/src/resources/js/layouts/restrict.vue';
import Settings from './components/settings.vue';
import DonorConnectManager from './components/settings/donorConnectManager.vue';
import Host from './components/settings/host.vue';
import Entity from './components/settings/host/entity.vue';
import SmartProcesses from './components/settings/host/smartprocesses.vue';

import LeadSupplier from './components/settings/host/b24EntityStrategies/lead.js';
import SmartProcSupplier from './components/settings/host/b24EntityStrategies/smart-process.js';

const meta = {
    enterClass: 'animate__animated animate__fadeInLeft',
    leaveClass: 'animate__animated animate__fadeOutRight',
}

export default [
    // { path: '/', redirect: '/leads'},
    { path: '/', component: Settings, name: 'settings', children: [
        {
            path: 'plus',
            component: DonorConnectManager,
            name: 'settings-host-plus'
        },
        {
            path: 'host/:hostId',
            redirect: { name: 'leads' }, // default child subcomponent
            component: Host,
            name: 'settings-specific-host',
            children: [
                {
                    path: 'leads',
                    component: Entity,
                    name: 'leads',
                    meta: {...meta, b24entity: LeadSupplier}
                },
                {
                    path: 'smartprocesses',
                    component: Entity,
                    name: 'smartprocesses',
                    meta: {...meta, b24entity: SmartProcSupplier}
                },
            ]
        },
    ] },
    { path: '/install', component: InstallPage },
    { path: '/restrict', component: RestrictionPage, 'name': 'restrict' },
];
