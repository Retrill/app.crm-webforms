import { reactive, provide, inject, readonly } from 'vue';

export const storeSymbol = Symbol('store');
// export const createState = () => reactive({ donorHosts: [] });

export const createStore = () => {
    const state = reactive({ donorHosts: [] });
    const updateDonorHosts = (donorHosts) => {
        for (const [i, newHost] of Object.entries(donorHosts)) {
            let found = false;
            for (const [j, existentHost] of Object.entries(state.donorHosts)) {
                if(newHost.id == existentHost.id){
                    found = true;
                    // we should save ractivity here, so should use Object.assign instead of ... = ...
                    Object.assign(state.donorHosts[j], newHost);
                }
            }
            if(!found){
                state.donorHosts.push(newHost);
            }
        }
        console.log('[state] donor hosts updated! ', state.donorHosts);
    };
    const removeDonorHost = (hostId) => {
        state.donorHosts.forEach(function(host, index, object) {
            if (host.id == hostId) {
                object.splice(index, 1);
            }
        });
    }

    return { updateDonorHosts, removeDonorHost, state: readonly(state) };
}

export const useStore = () => inject(storeSymbol);
export const provideStore = () => provide(
    storeSymbol,
    createStore()
);
