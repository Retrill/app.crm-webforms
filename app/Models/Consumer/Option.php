<?php

namespace App\Models\Consumer;

use Glu\B24AppBackground\Models\Consumer\ConsumerConnectionBased;

class Option extends ConsumerConnectionBased
{
    public $timestamps = false;
    protected $guarded = [];
    protected $casts = [
        'option' => 'json',
    ];

    public function value()
    {
        return $this->option['v'];
    }
}
