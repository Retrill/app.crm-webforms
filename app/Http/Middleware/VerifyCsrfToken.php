<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    use \Glu\B24AppBackground\Http\VerifyOrigin;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        'hosts/*',
        'receive',
    ];

    /**
     * The URIs that should be forced to be checked by CSRF verification.
     *
     * @var array
     */
    protected $forcedCheck = [
        'hosts/*/interact',
        'hosts/*/options/*',
    ];
}
