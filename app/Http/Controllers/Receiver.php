<?php
namespace App\Http\Controllers;

use App\Http\Cmd\BaseCommand;
use App\Models\Consumer\DonorHost;
use Illuminate\Http\Request;
use App\Http\SignedInteraction\ReceiveKernel;
use Webformat\Http\SignedInteraction\Receive\Director as ReceiveDirector;

class Receiver extends Controller
{
    public function __invoke(Request $request)
    {
        return (new ReceiveDirector(new ReceiveKernel($request)))->process();
    }
}
