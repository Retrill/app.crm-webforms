<?php

namespace App\Http\Controllers;

use App\Http\SignedInteraction\SendKernel;
use App\Models\Consumer\Option;
use Illuminate\Http\Request;
use Webformat\Http\SignedInteraction\Send\Director as SendDirector;
use App\Models\Consumer\DonorHost;

class Matches extends Controller
{
    public function index(int $id, Request $request)
    {
        $type = $request->input('type') ? ('_'.$request->input('type')) : '';
        $option = Option::firstWhere([
            'host_id' => $id,
            'name' => 'matches'.$type,
        ]);

        return [
            'status' => 'success',
            'data' => empty($option) ? [] : ($option->value() ?? []),
        ];
    }

    public function store(int $id, Request $request)
    {
        if (!$donorhost = DonorHost::find($id)) {
            return ['errors' => ['Host does not exist']];
        }
        $type = $request->input('type') ? ('_'.$request->input('type')) : '';
        $option = Option::firstOrNew([
            'host_id' => $id,
            'name' => 'matches'.$type,
        ]);
        $option->option = [
            'v' => $request->input('matches'),
        ];
        $option->save();

        if ('hh' != $donorhost->type) {
            // $webformIds = \array_unique(\array_column(\array_column(\array_column($request->input('matches'), 'from'), 'subfilter'), 'id'));
            $webformIds = [];
            $options = Option::where([
                ['host_id', $id],
                ['name', 'LIKE', '%matches%'],
            ])->get();
            foreach ($options as $option) {
                foreach ($option->value() as $match) {
                    if (!empty($match['active']) && (($match['from']['type'] ?? '') == 'webform')) {
                        $webformIds[] = $match['from']['subfilter']['id'];
                    }
                }
            }

            if ($errors = $this->passFormIds($id, \array_unique($webformIds), $request->input('member_id'))) {
                return [
                    'status' => 'error',
                    'data' => [],
                    'errors' => $errors,
                ];
            }
        }

        return [
            'status' => 'success',
            // 'data' => $matches->option ?: [],
            'data' => [],
        ];
    }

    protected function passFormIds(int $hostId, array $formIds, $endpointCode): ?array
    {
        $formIds = \array_map(fn (&$v) => (int) $v, $formIds);
        $sender = new SendDirector(new SendKernel(['host_id' => $hostId]));

        try {
            $remoteResults = $sender->send([
                'cmd' => 'Webformat\\FormTransmitter\\Http\\Cmd\\SaveFormIds',
                'params' => [
                    'member_id' => $endpointCode,
                    'ids' => $formIds,
                ],
            ]);
        } catch (\Throwable $er) {
            return [$er->getMessage()];
        }

        if (!$remoteResults = \json_decode($remoteResults, true)) {
            return ['The remote host returns a malformed JSON'];
        }
        if (!empty($remoteResults['errors'])) {
            return $remoteResults['errors'];
        }

        return null;
    }
}
