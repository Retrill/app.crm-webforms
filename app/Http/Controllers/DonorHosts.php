<?php

namespace App\Http\Controllers;

use App\Http\SignedInteraction\SendKernel;
use App\Models\Consumer\DonorHost;
use Glu\B24AppBackground\Db\Factories\EndpointFactory;
use Glu\B24RestSdk\Client\Consumer as ConsumerRestClient;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Webformat\Http\SignedInteraction\Send\Director as SendDirector;

class DonorHosts extends Controller
{
    /* public function __invoke(Request $request, ConsumerRestClient $client)
    {
        return $request->ip();

        return $request->server->all();

        return DonorHost::select('id', 'host', 'stage')->get();
    } */

    public function update(Request $request, int $id)
    {
        $donorhost = DonorHost::find($id);
        if (\is_null($donorhost)) {
            throw new \Exception('Host not found (preregistration required)!');
        }

        if ('initiated' != $donorhost->stage) {
            throw new \Exception('Host has been configured already!');
        }

        if ($request->ip() != $donorhost->ip && !\preg_match('/^172\.18\.0\.\d+$/', $request->ip())) {
            throw new \Exception('Incorrect IP address!');
        }
        if ($donorhost->key2enc) {
            throw new \Exception('Encryption key is already set!');
        }

        if ($request->input('key2dec')) {
            $donorhost->key2dec = $request->input('key2dec');
        }

        $keyPair = $this->tryGenerateKey();
        if (!\is_resource($keyPair)) {
            throw new \Exception('Can\'t generate keys!');
        }
        $key2enc = '';
        if (!\openssl_pkey_export($keyPair, $key2enc)) {
            throw new \Exception('Can\'t export private key!');
        }
        $donorhost->key2enc = $key2enc;
        $donorhost->salt = Str::randomDict(128, 'A-Z a-z \d %_', 'A-Z a-z \d');
        $donorhost->stage = 'accepted';
        $donorhost->save();

        return [
            'data' => [
                'salt' => $donorhost->salt,
                'key2dec' => \base64_encode(\openssl_pkey_get_details($keyPair)['key']),
            ],
        ];
    }

    public function store(Request $request)
    {
        $attrs = EndpointFactory::canonicalizeAttrs(['host' => $request->input('hostname')]);
        $attrs['ip'] = \gethostbyname($attrs['host']);
        if (!(bool) \preg_match('/^(\d+(\.|$)){4}/', $attrs['ip'])) {
            return ['errors' => ['Error resolving domain name']];
        }

        $donorhost = DonorHost::firstOrCreate(
            ['host' => $attrs['host']],
            ['ip' => $attrs['ip']]
        )->refresh(); // refreshign is needed to get actual model stage
        // \dump('$donorhost', $donorhost->toArray());

        if ('initiated' != $donorhost->stage) {
            return ['errors' => ['Incorrect host status (connected already?)']];
        }

        return ['data' => ['id' => $donorhost->id]];
    }

    public function show(int $id)
    {
        if (!$donorhost = DonorHost::find($id)) {
            return ['errors' => ['Host does not exist']];
        }
        if ('accepted' != $donorhost->stage) {
            return ['errors' => ['Host installation is not finished yet']];
        }

        return ['data' => [
            'finished' => true,
            'hostInfo' => [
                'id' => $id,
                'host' => $donorhost->host,
                'stage' => $donorhost->stage,
            ],
        ]];
    }

    protected function tryGenerateKey()
    {
        if (!\extension_loaded('openssl') || !\function_exists('openssl_pkey_new')) {
            return null;
        }

        $config = [
            'digest_alg' => 'sha512',
            'private_key_bits' => 4096,
            'private_key_type' => \OPENSSL_KEYTYPE_RSA,
        ];
        if (!$keyRes = \openssl_pkey_new($config)) {
            return null;
        }

        return $keyRes;
    }

    protected function interact(Request $request, int $id)
    {
        if (!$donorhost = DonorHost::find($id)) {
            return ['errors' => ['Specified channel not found']];
        }
        if($donorhost->type == 'hh'){
            $cmd = $request->input('payload')['cmd'];
            if(!\preg_match('/(^|\\\)(?<basename>[^\\\]+)$/', $cmd, $matches)){
                return [
                    'status' => 'error',
                    'errors' => ['Unexpected command syntax'],
                ];
            }
            $cmdBasename = \str_replace('..', '', $matches['basename']);
            $responsePath = \resource_path().'/js/channel/hh/'.$cmdBasename.'.json';
            if(!\file_exists($responsePath)){
                return [
                    'status' => 'error',
                    'errors' => ['Unexpected response filepath'],
                ];
            }
            return [
                'status' => 'success',
                'data' => \json_decode(\file_get_contents($responsePath), true)
            ];
        }

        $sender = new SendDirector(new SendKernel(['host_id' => $id]));
        // $sender->send($request->input('payload'));

        try {
            $remoteResults = $sender->send($request->input('payload'));
        } catch (\Throwable $er) {
            return [
                'status' => 'error',
                'errors' => ['Remote server error occured: '.$er->getMessage()],
            ];
        }

        if (!$remoteResults = \json_decode($remoteResults, true)) {
            return [
                'status' => 'error',
                'errors' => ['The remote host returns a malformed JSON'],
            ];
        }

        return $remoteResults;
    }

    public function destroy(Request $request, int $id)
    {
        if (!$donorhost = DonorHost::find($id)) {
            return ['errors' => ['Host does not exist']];
        }
        if ('accepted' == $donorhost->stage) {
            $sender = $this->constructSender($id);
            try {
                $remoteResults = $sender->send([
                    'cmd' => 'Webformat\\FormTransmitter\\Http\\Cmd\\Disconnect',
                    'params' => [
                        'member_id' => $request->input('member_id'),
                        'host_id' => $id,
                    ],
                ]);
            } catch (\Throwable $er) {
                return [
                    'status' => 'error',
                    'errors' => [$er->getMessage()],
                ];
            }

            if (!$remoteResults = \json_decode($remoteResults, true)) {
                return [
                    'status' => 'error',
                    'errors' => ['The remote host returns a malformed JSON'],
                ];
            }
            if (
                !empty($remoteResults['errors']) &&
                \array_filter($remoteResults['errors'], fn ($e) => !\is_array($e) || 'ONUB9j_B0hz3' != $e['code']) // "ONUB9j_B0hz3" = Specified member id not found
            ) {
                return [
                    'status' => 'error',
                    'errors' => \array_merge(['Remote server returned errors:'], $remoteResults['errors']),
                    // 'extra' => \preg_grep('/\#ONUB9j_B0hz3/', $remoteResults['errors']) //Specified member id not found
                ];
            }
        }

        $donorhost->delete();

        return [
            'status' => 'success',
            'data' => $remoteResults['data'] ?? [],
            'errors' => [],
            'warnings' => $remoteResults['errors'] ?? [],
        ];
    }

    protected function constructSender(string $hostId){
        return new SendDirector(new SendKernel(['host_id' => $hostId]));
    }
}
