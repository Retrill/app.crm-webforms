<?php

namespace App\Http\SignedInteraction\Cmd;

use App\Http\SignedInteraction\Cmd\ReceiveFormData\ValueAdapter;
use App\Models\Consumer\Option;
use Glu\B24AppBackground\Models\Consumer\Token;
use Glu\B24AppBackground\Models\Endpoint;
use Glu\B24RestSdk\Client as RestClient;
use Glu\B24RestSdk\Client\Consumer as ConsumerRestClient;
use Glu\B24RestSdk\Credentials\ModelBased as Credentials;
// use Webformat\Http\SignedInteraction\Receive\Cmd\BaseCommand;

class ReceiveFormData extends ReceiveMessage
{
    protected function isMessageValid(&$message)
    {
        if(!parent::{__FUNCTION__}($message)){
            return false;
        }
        if(!isset($message['identity']['id'])){
            $this->response['errors'][] = 'Undefined webform id';
            return false;
        }
        if(!isset($message['data']['form'])){
            $this->response['errors'][] = 'Undefined form data';
            return false;
        }

        return true;
    }

    protected function canonicalize(array &$message)
    {
        foreach ($message['data']['fields'] as $fieldCode => $values) {
            $canonicalValue = [];
            foreach ($values as $value) {
                if (!empty($value['USER_FILE_ID'])) {
                    $canonicalValue[$value['USER_FILE_ID']] = $value['USER_TEXT'];
                } else {
                    $canonicalValue[] = $value['USER_TEXT'];
                }
            }
            $message['data']['fields'][$fieldCode] = $canonicalValue;
        }
        $message['data']['fields']['TRACE'] = $message['extra']['trace'] ?: '';

        return true;
    }


    /* protected function send(int $formId, array $fields, array $form): array
    {
        $valueAdapter = (new ValueAdapter())->setSource('fields', \array_change_key_case($fields, \CASE_LOWER))->setSource('form', \array_change_key_case($form, \CASE_LOWER));
        $matches = Option::firstWhere('name', 'matches');
        $matches = $matches ? $matches->value() : [];
        $matches = $matches[$formId] ?? [];
        $dataAdapted = $this->adaptFormData($valueAdapter, $matches);
        $args = [
            'fields' => $dataAdapted + ['TRACE' => $fields['TRACE']],
            'params' => ['REGISTER_SONET_EVENT' => 'N'],
        ];
        $results = $this->client->exec('crm.lead.add', $args);
        if ($errors = $this->client->getErrors()) {
            throw new \Exception('Remote save results error! '.var_export($errors, true));
        }
        return $results;
    } */

    /* protected function adaptFormData(ValueAdapter $valueAdapter, array $matches)
    {
        $adapters = [];
        $adapted = [];
        $targetSpecification = \current($this->client->exec('crm.lead.fields', []));
        if ($errors = $this->client->getErrors()) {
            throw new \Exception('Can\'t get target specification! '.var_export($errors, true));
        }
        $targetSpecification = $targetSpecification['result'];

        if (empty($targetSpecification)) {
            throw new \Exception('Can\'t receive target specification (http request error?). Client errors: '.var_export($this->client->getErrors(), true));
        }
        foreach ($matches as $match) {
            $targetCode = $match['target'];
            if (!isset($adapters[$targetCode])) {
                $adapters[$targetCode] = clone $valueAdapter;
            }
            $adapters[$targetCode]->setSpecification($targetSpecification[$targetCode])->addSourceValue($match['source']);
        }
        foreach ($adapters as $targetCode => $fieldAdapter) {
            $adapted[$targetCode] = $fieldAdapter->getAdaptedValue();
        }

        return $adapted;
    } */
}
