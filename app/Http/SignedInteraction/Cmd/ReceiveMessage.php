<?php

namespace App\Http\SignedInteraction\Cmd;

use App\Http\SignedInteraction\Cmd\ReceiveMessage\Destination\Base as BaseDestination;
use App\Http\SignedInteraction\Cmd\ReceiveMessage\ValueAdapter;
use App\Models\Consumer\Option;
use Glu\B24AppBackground\Models\Consumer\Token;
use Glu\B24AppBackground\Models\Endpoint;
use Glu\B24RestSdk\Client as RestClient;
use Glu\B24RestSdk\Client\Consumer as ConsumerRestClient;
use Glu\B24RestSdk\Credentials\ModelBased as Credentials;
use Webformat\Http\SignedInteraction\Receive\Cmd\BaseCommand;

class ReceiveMessage extends BaseCommand
{
    protected RestClient $client;
    protected $donorHostId;

    public function __construct(object $request)
    {
        parent::{__FUNCTION__}($request);
        $this->donorHostId = (int) $request->input('donor_host_id');
        $endpoint = Endpoint::firstWhere('code', $request->input('member_id'));
        $token = Token::firstWhere('system', 1);
        if (\is_null($endpoint) || \is_null($token)) {
            throw new \Exception('Credentials not found');
        }
        // we should manually define Credentials here (not from request!)
        $this->client = new ConsumerRestClient(Credentials::makeInstance($endpoint, $token));
        // $this->client = \app(ConsumerRestClient::class);
    }

    public function __invoke(...$params): array
    {
        $message = $params[0]; // type, identity, data, extra, specification
        if (!$this->isMessageValid($message)) {
            throw new \Exception('The message is not valid');
        }

        $this->canonicalize($message);

        // извлечь наборы соответствий, подходящие по типу исходной сущности. Если нет - прекратить обработку
        // сгруппировать наборы соответствий по типу целевой сущности
        if (!$matches = $this->getMatches($message)) {
            throw new \Exception('No matches assigned for this message (nothing to do)');
        }

        $valueAdapterProto = new ValueAdapter();
        foreach ($message['data'] as $groupCode => $fields) {
            $fields = \array_change_key_case($fields, \CASE_LOWER);
            // sources are passed by ref and will be shared between all of the clones of this proto object
            $valueAdapterProto->setSource($groupCode, $fields);
            unset($fields); //very important! We should unlink variable in foreach
        }

        $specifications = [];
        $addResults = [];
        foreach ($matches as $group) {
            $destEntityType = \str_replace(' ', '', \ucwords(\preg_replace('/[^\w]+/', ' ', $group['to']['type'])));
            $destEntityType = 'App\\Http\\SignedInteraction\\Cmd\\ReceiveMessage\\Destination\\'.$destEntityType;

            if (!\class_exists($destEntityType)) {
                throw new \Exception('Inexistent destination class "'.$destEntityType.'"!');
            }
            if (!\is_subclass_of($destEntityType, BaseDestination::class)) {
                throw new \Exception('Unexpected destination classname "'.$destEntityType.'"!');
            }
            $destination = new $destEntityType($this->client, $group['to']['subfilter']);
            $valueAdapterProto->setFormDataMode($destination->withFileDataKey());
            //  - извлечь спецификацию целевой сущности
            $specification = $destination->getSpecification();
            $specifications[$destEntityType] = $specification;

            foreach ($group['pairsets'] as $pairs) {
                $adaptedData = $this->getAdaptedData($valueAdapterProto, $pairs, $specification);
                if (isset($message['extra']['trace'])) {
                    $adaptedData[$destination->getTraceKey()] = $message['extra']['trace'];
                }

                $addResults[] = $destination->add($adaptedData);
            }
        }

        return [
            'status' => 'success',
            // 'matches' => $matches,
            // 'specifications' => $specifications,
            // 'adapted' => $adapted,
            'data' => $addResults,
        ];

        //  - подготовить набор данных (полей) для отправки в Б24 (адаптировать данные под ожидаемый Б24 формат)
        //  - выполнить отправку в Б24

        return $results;
    }

    protected function canonicalize(array &$message)
    {
        return true;
    }

    protected function isMessageValid(&$message)
    {
        if (empty($message['type']) || ('message' != $message['type'])) {
            throw new \Exception('Channel-unexpected data type! "message" expected and "'.$message['type'].'" given');
        }
        if (!isset($message['identity']['type'])) {
            throw new \Exception('Undefined identity type');
        }
        if (empty($message['data'])) {
            throw new \Exception('Empty message data');
        }

        return true;
    }

    protected function getMatches(array $message)
    {
        $search = $message['identity'];
        $identityType = $message['identity']['type'];
        unset($search['type']);

        $groupedMatches = []; // group by target entity type && filter
        $options = Option::where([
            ['host_id', $this->donorHostId],
            ['name', 'LIKE', '%matches%'],
        ])->get();
        foreach ($options as $option) {
            foreach ($option->value() as $match) {
                if (
                    (($match['from']['type'] ?? '') == $identityType) &&
                    ($match['from']['subfilter'] == $search)
                ) {
                    $this->insertMatch($match, $groupedMatches);
                }
            }
        }

        return $groupedMatches;
    }

    protected function insertMatch(&$candidate, &$groupedMatches): void
    {
        $inserted = false;
        foreach ($groupedMatches as &$group) {
            if ($group['to'] == $candidate['to']) {
                $group['pairsets'][] = $candidate['pairs'];
                $inserted = true;
                break;
            }
        }
        if (!$inserted) {
            $groupedMatches[] = [
                'to' => $candidate['to'],
                'pairsets' => [
                    $candidate['pairs'],
                ],
            ];
        }
    }

    /* protected function send(int $formId, array $fields, array $form): array
    {
        $valueAdapter = (new ValueAdapter())->setSource('fields', \array_change_key_case($fields, \CASE_LOWER))->setSource('form', \array_change_key_case($form, \CASE_LOWER));
        $matches = Option::firstWhere('name', 'matches');
        $matches = $matches ? $matches->value() : [];
        $matches = $matches[$formId] ?? [];
        $dataAdapted = $this->adaptFormData($valueAdapter, $matches);
        $args = [
            'fields' => $dataAdapted + ['TRACE' => $fields['TRACE']],
            'params' => ['REGISTER_SONET_EVENT' => 'N'],
        ];
        $results = $this->client->exec('crm.lead.add', $args);
        if ($errors = $this->client->getErrors()) {
            $errors = ['Remote save results error!', ...$errors];
            throw new \Exception(\json_encode($errors, \JSON_UNESCAPED_UNICODE));
        }

        return $results;
    } */

    protected function getAdaptedData(ValueAdapter $adapterProto, array $matchPairs, array &$targetSpecification)
    {
        $adapters = [];
        $adapted = [];

        foreach ($matchPairs as $pair) {
            $upperCaseCode = $pair['target'];
            if (!isset($targetSpecification[$upperCaseCode])) {
                // target field does not exist (already), ignoring it
                continue;
            }
            $canonicalCode = $targetSpecification[$upperCaseCode]['canonicalCode'] ?? $upperCaseCode;
            if (!isset($adapters[$canonicalCode])) {
                $adapters[$canonicalCode] = clone $adapterProto;
            }
            $adapters[$canonicalCode]
                ->setTargetSpecification($targetSpecification[$upperCaseCode])
                ->addSourceValue($pair['source']);
        }
        foreach ($adapters as $canonicalCode => $fieldAdapter) {
            $adapted[$canonicalCode] = $fieldAdapter->getAdaptedValue();
        }

        return $adapted;
    }
}
