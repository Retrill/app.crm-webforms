let message =
{
    "type": "message", // В будущем возможно, что будет отправляться пакет из нескольких сообщений с блоком общей информации или что-то такое
    "identity": {
        "type": "webform",
        "id": 7
    },
    "data": {
        "form": {
            "ID": 7,
            "NAME": "Заявка с сайта"
        },
        "fields": {
            "fieldCode1": [
                {
                    "USER_TEXT": "value1"
                },
                {
                    "USER_TEXT": "value2"
                }
            ],
            "fieldCode2": {}
        },
        "result": {

        },
    },
    "extra": {
        "trace": '{"url":"https://72-brucite.wfdemo.ru/wftest.php?WEB_FORM_ID=7","device":{"isMobile":false},"tags":{"ts":1640860328,"list":{},"gclid":null},"client":{"gaId":"151173032.1632388234","yaId":"1632388234285454802"},"pages":{"list":[["https://72-brucite.wfdemo.ru/result_edit.php?WEB_FORM_ID=7&RESULT_ID=1112&formresult=addok",1642061677,"Страница не найдена"],["https://72-brucite.wfdemo.ru/result_edit.php?WEB_FORM_ID=7&RESULT_ID=1113&formresult=addok",1642062011,"404 Not Found"],["https://72-brucite.wfdemo.ru/result_edit.php?WEB_FORM_ID=7&RESULT_ID=1114&formresult=addok",1642062330,"404 Not Found"],["https://72-brucite.wfdemo.ru/result_edit.php?WEB_FORM_ID=7&RESULT_ID=1115&formresult=addok",1642062738,"404 Not Found"],["https://72-brucite.wfdemo.ru/wftest.php?WEB_FORM_ID=7",1643010858,"wftest"]]},"gid":null,"previous":{"list":[]}}'
    },

    // Опциональное поле. Если задано, экономит запросы
    "specification": {}
}