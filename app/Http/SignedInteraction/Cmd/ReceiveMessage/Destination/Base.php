<?php

namespace App\Http\SignedInteraction\Cmd\ReceiveMessage\Destination;

use Glu\B24RestSdk\Client as RestClient;

class Base
{
    protected RestClient $client;
    protected array $filter;
    protected string $restEntityName;

    public function __construct(RestClient $client, array $filter = [])
    {
        $this->client = $client;
        // $this->filter = \array_diff_key($filter, \array_flip(['type']));
        $this->filter = $filter;
        if (empty($this->restEntityName)) {
            \preg_match('/(^|\\\)(?<basename>[^\\\]+)$/', static::class, $matches);
            $this->restEntityName = \strtolower($matches['basename']);
        }
    }

    public function getSpecification()
    {
        $targetSpecification = \current($this->client->exec('crm.'.$this->restEntityName.'.fields', $this->filter));
        if ($errors = $this->client->getErrors()) {
            throw new \Exception(\json_encode(['Can\'t get target specification '.$this->restEntityName.'!', ...$errors]));
        }
        $targetSpecification = $targetSpecification['result']['fields'] ?? $targetSpecification['result'];

        if (empty($targetSpecification)) {
            throw new \Exception('Empty target specification result. Entity = '.static::class);
        }

        return $targetSpecification;
    }

    public function add(array $fields)
    {
        $args = [
            'fields' => $fields,
            'params' => ['REGISTER_SONET_EVENT' => 'N'],
        ] + $this->filter;

        $methodName = 'crm.'.$this->restEntityName.'.add';

        // \wf\dump($args, $methodName);

        $results = $this->client->exec($methodName, $args);
        if ($errors = $this->client->getErrors()) {
            $errors = ['Remote save results error!', ...$errors];
            throw new \Exception(\json_encode($errors, \JSON_UNESCAPED_UNICODE));
        }

        return $results;
    }

    public function getTraceKey()
    {
        return 'TRACE';
    }

    public function withFileDataKey()
    {
        return true;
    }
}
