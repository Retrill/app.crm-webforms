<?php

namespace App\Http\SignedInteraction\Cmd\ReceiveMessage\Destination;

class SmartProcess extends Base
{
    protected string $restEntityName = 'item';

    /* public function getSpecification()
    {
        return parent::{__FUNCTION__}()
        $bitrixFormatSpecification = parent::{__FUNCTION__}();
        $canonicalSpecification = [];
        foreach ($bitrixFormatSpecification as $camelCode => $description) {
            $canonicalSpecification[$description['upperName']] = $description + ['canonicalCode' => $camelCode];
        }

        return $canonicalSpecification;
    } */

    public function getTraceKey()
    {
        return 'trace';
    }

    public function withFileDataKey()
    {
        return true;
    }
}
