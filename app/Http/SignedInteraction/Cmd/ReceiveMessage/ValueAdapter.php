<?php

namespace App\Http\SignedInteraction\Cmd\ReceiveMessage;

class ValueAdapter
{
    protected $specification;
    public $sources = [];
    protected $value = [];
    protected $withFileDataKey = true;

    public function __construct()
    {
    }

    public function setSource(string $sourceCode, array &$data)
    {
        $this->sources[$sourceCode] = &$data;

        return $this;
    }

    public function setTargetSpecification(array $specification)
    {
        $this->specification = $specification;

        return $this;
    }

    public function addSourceValue(string $template)
    {
        $regex = '/\{\{\s*(?<group>\S+)\.(?<param>\S+)\s*\}\}/';
        if (!(bool) \preg_match_all($regex, $template, $matches, \PREG_SET_ORDER)) {
            // this is a static value
            $this->value[] = \trim($template);

            return $this;
        }

        $textWithoutVars = \trim(\preg_replace($regex, '', $template));
        $flattenMode = (bool) $textWithoutVars;
        $flattenValue = \trim($template);

        foreach ($matches as $match) {
            $groupCode = $match['group'];
            $paramCode = \mb_strtolower($match['param'], 'UTF-8');
            $sourceValue = (array) ($this->sources[$groupCode][$paramCode] ?? []);

            if ($flattenMode) {
                $macro = $match[0]; // full match
                if (\is_array($sourceValue)) {
                    \sort($sourceValue);
                    $sourceValue = \implode(' / ', $sourceValue);
                }
                $flattenValue = \str_replace($macro, $sourceValue, $flattenValue);
            } else {
                if (\in_array($this->specification['type'], ['file', 'image'], true)) {
                    foreach($sourceValue as $fileId => $fileTitle){
                        // $fileId = (int) $fileId;
                        if(/*$fileId < 0 &&*/ (bool)\preg_match('/^https?\:\/\//i', $fileTitle)){
                            // save url to file with a negative file id to be downloaded later;
                            $this->value[\min(\array_keys($this->value ?: [0])) - 1] = $fileTitle;
                        }else{
                            $this->value[$fileId] = $fileTitle;
                        }
                    }
                    // $this->value += $sourceValue; //fileId => fileTitle
                } else {
                    $this->value = \array_merge($this->value, $sourceValue);
                }
            }
        }

        if ($flattenMode) {
            $this->value[] = $flattenValue;
        }

        return $this;
    }

    public function getAdaptedValue()
    {
        $result = [];
        if(\is_array($this->value)){
            $this->value = \array_filter(\array_map('trim', $this->value));
        }
        if ('crm_multifield' == $this->specification['type']) {
            foreach ((array) $this->value as $value) {
                $result[] = ['VALUE' => $value, 'VALUE_TYPE' => 'WORK'];
            }

            return $result;
        }
        // $isMultiple = !empty($this->specification['isMultiple']) || !empty($this->specification['ismultiple']);
        $isMultiple = !empty($this->specification['ismultiple']);
        if (\in_array($this->specification['type'], ['file', 'image'], true)) {
            foreach ((array) $this->value as $fileId => $fileTitle) {
                if ($fileId >= 0){
                    if (!($filepath = request()->file()['attachments'][$fileId] ?? '')) {
                        return false;
                    }
                    $fileContents = \file_get_contents($filepath);
                }else{
                    // file title = url of the remote file
                    $fileContents = \file_get_contents($fileTitle);
                }

                $value = [
                    ($fileId >= 0) ? $fileTitle : \preg_replace('/\?.*?$/', '', \basename($fileTitle)),
                    \base64_encode($fileContents),
                ];
                $result[] = $this->withFileDataKey ? ['fileData' => $value] : $value;
            }

            return $isMultiple ? $result : \current($result);
        }

        return $isMultiple ? (array) $this->value : \implode(' / ', (array) $this->value);
    }

    public function setFormDataMode(bool $mode)
    {
        $this->withFileDataKey = $mode;
    }
}
