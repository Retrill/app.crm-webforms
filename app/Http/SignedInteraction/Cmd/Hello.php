<?php

namespace App\Http\SignedInteraction\Cmd;

use Webformat\Http\SignedInteraction\Receive\Cmd\BaseCommand;

class Hello extends BaseCommand
{
    public function __invoke(...$params): array
    {
        $response = [
            'hello' => $params[0],
            'attachments' => [],
        ];

        foreach ($this->attachments as $index => $attachment) {
            $response['attachments'][$index] = \md5_file($attachment->path());
        }

        return $response;
    }
}
