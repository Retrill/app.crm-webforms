<?php

namespace App\Http\SignedInteraction;

use App\Models\Consumer\DonorHost;
use GuzzleHttp\Client as HttpClient;
use Webformat\Http\SignedInteraction\Send\Kernel;

class SendKernel extends Kernel
{
    protected ?DonorHost $donorHost;
    protected HttpClient $http;

    public function __construct(array $runtimeOpts)
    {
        parent::{__FUNCTION__}($runtimeOpts);

        if (empty($this->runtime['host_id']) || !\is_numeric($this->runtime['host_id'])) {
            throw new \Exception('Host id is empty, the request can\'t be sent!');
        }

        $this->donorHost = DonorHost::find((int) $this->runtime['host_id']);
        if (\is_null($this->donorHost)) {
            throw new \Exception('Host not found!');
        }
        if ('accepted' != $this->donorHost->stage) {
            throw new \Exception('Host installation isn\'t completed!');
        }
        $this->http = new HttpClient();

        $_this = &$this;
        $this->addCallback('about_to_send', function (array &$request) use (&$_this) {
            $request += [
                'member_id' => \request()->input('member_id') //$_this->donorHost['member_id'],
            ];
        });
    }

    public function getEndpoint(): string
    {
        return 'https://'.$this->donorHost->host.'/bitrix/services/main/ajax.php?action=webformat:formtransmitter.entrypoint.receive';
    }

    public function getKey2Enc(): ?string
    {
        return $this->donorHost->key2enc;
    }

    public function getSalt(): ?string
    {
        return $this->donorHost->salt;
    }

    public function post(string $url, array $data, array $attachments, &$responseCode): ?string
    {
        if (!empty($attachments)) {
            throw new \Exception('This version of the script does not support file attachments!');
        }
        try {
            $response = $this->http->post($url, ['form_params' => $data]);
        } catch (\Exception $er) {
            $response = $er->getResponse();
        }
        $responseCode = (int) $response->getStatusCode();

        $responseText = (string) $response->getBody();
        // \dd('$responseText: ', $responseText);
        return $responseText;
    }
}
