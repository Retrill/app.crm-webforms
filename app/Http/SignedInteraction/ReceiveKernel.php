<?php

namespace App\Http\SignedInteraction;

use App\Models\Consumer\DonorHost;
use Webformat\Http\SignedInteraction\Receive\Kernel;
use Illuminate\Http\Request;

class ReceiveKernel extends Kernel
{
    protected ?DonorHost $donorHost;
    protected ?array $attachments;

    public function requestIsValid(): bool
    {
        if(!parent::{__FUNCTION__}()){
            return false;
        }

        if(!($this->request instanceof Request) && !\is_subclass_of($this->request, Request::class))
        {
            $this->response['status'] = 'error';
            $this->response['errors'][] = 'Unexpected request type';
            return false;
        }

        return true;
    }

    public function init(): bool
    {
        $donorHostId = (int) $this->request->input('donor_host_id');
        if ($donorHostId <= 0) {
            $this->response['status'] = 'error';
            $this->response['errors'][] = 'Empty donor host id';

            return false;
        }

        if (
            (!$this->donorHost = DonorHost::find($donorHostId)) ||
            ('accepted' != $this->donorHost->stage)
        ) {
            $this->response['status'] = 'error';
            $this->response['errors'][] = 'Specified donor host not found';

            return false;
        }

        $this->attachments = $this->request->file()['attachments'] ?? [];

        return true;
    }

    public function getKey2Dec(): ?string
    {
        return $this->donorHost->key2dec;
    }

    public function getRequestString(string $name): ?string
    {
        return $this->request->input($name);
    }

    public function getSalt(): ?string
    {
        return $this->donorHost->salt;
    }

    public function getRequestAttachments(): array
    {
        return $this->attachments ?: [];
    }

    public function attachIsValid(&$requestAttach, string $hashExpected): bool
    {
        return \md5_file($requestAttach->path()) == $hashExpected;
    }
}
