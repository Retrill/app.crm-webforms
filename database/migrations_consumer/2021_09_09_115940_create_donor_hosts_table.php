<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDonorHostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('donor_hosts', function (Blueprint $table) {
            $table->id();
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at')->useCurrent()->useCurrentOnUpdate();
            $table->string('host', 512)->nullable(true)->unique();
            $table->string('title', 512)->nullable(false)->default('');
            $table->enum('type', ['webform', 'hh'])->nullable(false)->default('webform');
            $table->ipAddress('ip')->nullable(false);
            $table->enum('stage', ['initiated', 'accepted'])->nullable(false)->default('initiated');
            // private key to encrypt outgoing requests to this donor remote host
            $table->string('salt', 128)->nullable(false)->default('');
            $table->text('key2enc')->nullable(true);
            // public key to decrypt incoming requests from this donor remote host
            $table->text('key2dec')->nullable(true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('donor_hosts');
    }
}
